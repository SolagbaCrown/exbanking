## ExBanking

The requirements can be found [here](Requirements.md)

### Modules
#### ExBanking
Exposes the public methods: `create_user/1`, `deposit/3`, `withdraw/3`, `get_balance/2`, `send/4 `

#### User
Implements the methods the public methods: `create_user/1`, `deposit/3`, `withdraw/3`, `get_balance/2`, `send/4 `

#### Formatter
Rounds `number` to 2 precision point.

### GenServer
Handles the `user operations`.

### Registry
Stores the `states` and `processes`.

### RateLimiter
Controls the allowed maximum requests.


### Testing
Run all tests with `mix test`

Run separately 
    run `iex -S mix`, 
    then run `import ExBanking`, 
    then test each function for example `create_user("any_user")`


### Note
*Money could have been handled using Integer and stored as cents*
