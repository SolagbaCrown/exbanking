defmodule ExBanking.User.Supervisor do
  @moduledoc false
  use DynamicSupervisor

  alias ExBanking.GenServerApi

  @spec start_link(any) :: :ignore | {:error, any} | {:ok, pid}
  def start_link(_) do
    DynamicSupervisor.start_link(__MODULE__, [], name: __MODULE__)
  end

  @spec init(any) ::
          {:ok,
           %{
             extra_arguments: list,
             intensity: non_neg_integer,
             max_children: :infinity | non_neg_integer,
             period: pos_integer,
             strategy: :one_for_one
           }}
  def init(_) do
    DynamicSupervisor.init(strategy: :one_for_one)
  end

  @spec start_child(any) :: :ignore | {:error, any} | {:ok, pid} | {:ok, pid, any}
  def start_child(user) do
    DynamicSupervisor.start_child(
      __MODULE__,
      {GenServerApi, {:via, Registry, {Registry.ExBanking, user}}}
    )
  end
end
