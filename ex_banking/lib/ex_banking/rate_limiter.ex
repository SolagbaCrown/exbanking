defmodule ExBanking.RateLimiter do
  @moduledoc """
  This controls the allowed max concurrent requests.
  """

  @spec update_request_count(map) :: map
  def update_request_count(state) do
    Map.update(state, :request_count, 0, fn count -> count + 1 end)
  end
end
