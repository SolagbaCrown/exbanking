defmodule ExBanking.Application do
  @moduledoc false

  use Application

  @spec start(any, any) :: {:error, any} | {:ok, pid}
  def start(_type, _args) do
    ExBanking.Supervisor.start_link([])
  end
end
