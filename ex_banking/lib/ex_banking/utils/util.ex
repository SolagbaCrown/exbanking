defmodule ExBanking.Utils.Util do
  @moduledoc """
  This is a common utility class
  """

  alias ExBanking.Formatter

  @spec get_user(any) :: [{pid, any}]
  def get_user(user) do
    Registry.lookup(Registry.ExBanking, user)
  end

  @spec get_balance_from_response(map, any) :: {:ok, any}
  def get_balance_from_response(state, currency) do
    balance = Map.get(state, currency)

    if is_number(balance) do
      {:ok, Formatter.format(balance)}
    else
      {:ok, Formatter.format(0)}
    end
  end

  @spec sufficient_fund?(map, any, any) :: boolean
  def sufficient_fund?(state, currency, amount) do
    available_balance = Map.get(state, currency)

    available_balance != nil && available_balance >= amount
  end
end
