defmodule ExBanking.GenServerApi do
  @moduledoc """
  This contains the GenServerAPI module which implements the required GenServer methods.
  """

  @type ex_banking_error :: ExBanking.ex_banking_error()
  @type ex_banking_success :: ExBanking.ex_banking_success()
  @request_count :request_count
  use GenServer
  alias ExBanking.RateLimiter
  alias ExBanking.Utils.Util

  @spec call(atom | pid | {atom, any} | {:via, atom, any}, any) :: any
  def call(user_pid, user_operation) do
    GenServer.call(user_pid, user_operation)
  end

  @spec start_link(atom | {:global, any} | {:via, atom, any}) ::
          :ignore | {:error, any} | {:ok, pid}
  def start_link(user) do
    GenServer.start_link(__MODULE__, %{@request_count => 0}, name: user)
  end

  @spec init(any) :: {:ok, any}
  def init(state) do
    {:ok, state}
  end

  def handle_call(
        {:deposit, amount, currency},
        _from,
        state = %{@request_count => request_count}
      )
      when request_count < 10 do
    new_state =
      state
      |> RateLimiter.update_request_count()
      |> Map.update(currency, amount, fn balance -> add(balance, amount) end)

    {:reply, new_state, new_state}
  end

  def handle_call(
        {:withdraw, amount, currency},
        _from,
        state = %{@request_count => request_count}
      )
      when request_count < 10 do
    if Util.sufficient_fund?(state, currency, amount) do
      new_state =
        state
        |> RateLimiter.update_request_count()
        |> Map.update(currency, amount, fn balance -> subtract(balance, amount) end)

      {:reply, new_state, new_state}
    else
      {:reply, :not_enough_money, state}
    end
  end

  def handle_call({:get_balance}, _from, state = %{@request_count => request_count})
      when request_count < 10 do
    new_state = RateLimiter.update_request_count(state)

    {:reply, new_state, new_state}
  end

  def handle_call(
        {:send, to_pid, amount, currency},
        _from,
        state = %{@request_count => request_count}
      )
      when request_count < 10 do
    with true <- Util.sufficient_fund?(state, currency, amount),
         {:ok, to_user_balance} <- deposit_amount(to_pid, amount, currency) do
      new_state =
        state
        |> RateLimiter.update_request_count()
        |> Map.update(currency, amount, fn balance -> subtract(balance, amount) end)

      {:ok, from_user_balance} = Util.get_balance_from_response(new_state, currency)
      {:reply, {:ok, from_user_balance, to_user_balance}, new_state}
    else
      false ->
        {:reply, :not_enough_money, state}

      {:error, :too_many_requests_to_receiver} ->
        {:reply, {:error, :too_many_requests_to_receiver}, state}
    end
  end

  def handle_call(_args, _from, state) do
    {:reply, :too_many_requests_to_user, state}
  end

  defp deposit_amount(to_user_pid, amount, currency) do
    with %{} = new_state <- call(to_user_pid, {:deposit, amount, currency}),
         {:ok, to_user_balance} <- Util.get_balance_from_response(new_state, currency) do
      {:ok, to_user_balance}
    else
      :too_many_requests_to_user -> {:error, :too_many_requests_to_receiver}
    end
  end

  defp add(a, b) do
    a + b
  end

  defp subtract(a, b) do
    a - b
  end
end
