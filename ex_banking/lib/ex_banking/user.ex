defmodule ExBanking.User do
  @moduledoc """
  This contains the user module which implements the public API.
  """
  @type ex_banking_error :: ExBanking.ex_banking_error()
  @type ex_banking_success :: ExBanking.ex_banking_success()

  alias ExBanking.User.Supervisor
  alias ExBanking.GenServerApi
  alias ExBanking.Utils.Util

  @spec create_user(user :: String.t()) :: ex_banking_success | ex_banking_error
  def create_user(user) do
    case Util.get_user(user) do
      [] ->
        Supervisor.start_child(user)
        :ok

      [{_user_pid, _state}] ->
        {:error, :user_already_exists}
    end
  end

  @spec deposit(user :: String.t(), amount :: number, currency :: String.t()) ::
          ex_banking_success | ex_banking_error
  def deposit(user, amount, currency) do
    with [{user_pid, _state}] <- Util.get_user(user),
         %{} = response_balance <- GenServerApi.call(user_pid, {:deposit, amount, currency}) do
      Util.get_balance_from_response(response_balance, currency)
    else
      [] -> {:error, :user_does_not_exist}
      :too_many_requests_to_user -> {:error, :too_many_requests_to_user}
    end
  end

  @spec withdraw(user :: String.t(), amount :: number, currency :: String.t()) ::
          ex_banking_success | ex_banking_error
  def withdraw(user, amount, currency) do
    with [{user_pid, _state}] <- Util.get_user(user),
         %{} = response_balance <- GenServerApi.call(user_pid, {:withdraw, amount, currency}) do
      Util.get_balance_from_response(response_balance, currency)
    else
      [] -> {:error, :user_does_not_exist}
      :not_enough_money -> {:error, :not_enough_money}
      :too_many_requests_to_user -> {:error, :too_many_requests_to_user}
    end
  end

  @spec get_balance(user :: String.t(), currency :: String.t()) ::
          ex_banking_success | ex_banking_error
  def get_balance(user, currency) do
    with [{user_pid, _state}] <- Util.get_user(user),
         %{} = response_balance <- GenServerApi.call(user_pid, {:get_balance}) do
      Util.get_balance_from_response(response_balance, currency)
    else
      [] -> {:error, :user_does_not_exist}
      :too_many_requests_to_user -> {:error, :too_many_requests_to_user}
    end
  end

  @spec send(
          from_user :: String.t(),
          to_user :: String.t(),
          amount :: number,
          currency :: String.t()
        ) ::
          ex_banking_success | ex_banking_error
  def send(from_user, to_user, amount, currency) do
    from_user = Util.get_user(from_user)
    to_user = Util.get_user(to_user)

    cond do
      from_user == [] -> {:error, :sender_does_not_exist}
      to_user == [] -> {:error, :receiver_does_not_exist}
      true -> send_amount(from_user, to_user, amount, currency)
    end
  end

  defp send_amount(
         [{from_user_pid, _from_user_state}],
         [{to_user_pid, _to_user_state}],
         amount,
         currency
       ) do
    withdraw_amount(from_user_pid, to_user_pid, amount, currency)
  end

  defp withdraw_amount(from_user_pid, to_user_pid, amount, currency) do
    case GenServerApi.call(from_user_pid, {:send, to_user_pid, amount, currency}) do
      {:ok, from_reply, to_reply} -> {:ok, from_reply, to_reply}
      :not_enough_money -> {:error, :not_enough_money}
      :too_many_requests_to_user -> {:error, :too_many_requests_to_sender}
      {:error, :too_many_requests_to_receiver} -> {:error, :too_many_requests_to_receiver}
    end
  end
end
