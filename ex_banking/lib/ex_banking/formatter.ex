defmodule ExBanking.Formatter do
  @moduledoc """
  This validates and format the amount.
  """

  @spec format(number) :: float
  @doc """
  - Money amount of any currency should not be negative.
  - Application should provide 2 decimal precision of money amount for any currency.
  """
  def format(amount) when is_number(amount) and amount >= 0 do
    if is_float(amount) do
      amount
      |> Float.round(2)
    else
      amount
      |> to_string()
      |> Float.parse()
      |> elem(0)
      |> Float.round(2)
    end
  end
end
