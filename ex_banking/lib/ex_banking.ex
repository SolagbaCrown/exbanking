defmodule ExBanking do
  @moduledoc """
  This contains the definition of the public API for ExBanking.

  It calls the User module to handle the implementation.

  It also handles some validations.
  """

  alias ExBanking.User

  @type ex_banking_error ::
          {:error,
           :wrong_arguments
           | :user_already_exists
           | :user_does_not_exist
           | :too_many_requests_to_user
           | :not_enough_money
           | :sender_does_not_exist
           | :receiver_does_not_exist
           | :too_many_requests_to_sender
           | :too_many_requests_to_receiver}

  @type ex_banking_success ::
          :ok
          | {:ok, new_balance :: number}
          | {:ok, from_user_balance :: number, to_user_balance :: number}

  @doc """
  - Function creates new user in the system
  - New user has zero balance of any currency
  """
  @spec create_user(user :: String.t()) :: ex_banking_success | ex_banking_error
  def create_user(""), do: {:error, :wrong_arguments}
  def create_user(<<user::binary>>), do: User.create_user(user)
  def create_user(_user), do: {:error, :wrong_arguments}

  @doc """
  - Increases user's balance in given currency by amount value
  - Returns new_balance of the user in given format
  """
  @spec deposit(user :: String.t(), amount :: number, currency :: String.t()) ::
          ex_banking_success | ex_banking_error
  def deposit("", _amount, _currency), do: {:error, :wrong_arguments}
  def deposit(_user, _amount, ""), do: {:error, :wrong_arguments}

  def deposit(<<user::binary>>, amount, <<currency::binary>>)
      when is_number(amount) and amount >= 0,
      do: User.deposit(user, amount, currency)

  def deposit(_user, _amount, _currency), do: {:error, :wrong_arguments}

  @doc """
  - Decreases user's balance in given currency by amount value
  - Returns new_balance of the user in given format
  """
  @spec withdraw(user :: String.t(), amount :: number, currency :: String.t()) ::
          ex_banking_success | ex_banking_error
  def withdraw("", _amount, _currency), do: {:error, :wrong_arguments}
  def withdraw(_user, _amount, ""), do: {:error, :wrong_arguments}

  def withdraw(<<user::binary>>, amount, <<currency::binary>>)
      when is_number(amount) and amount >= 0,
      do: User.withdraw(user, amount, currency)

  def withdraw(_user, _amount, _currency), do: {:error, :wrong_arguments}

  @doc """
  - Returns balance of the user in given format
  """
  @spec get_balance(user :: String.t(), currency :: String.t()) ::
          ex_banking_success | ex_banking_error
  def get_balance("", _currency), do: {:error, :wrong_arguments}
  def get_balance(_user, ""), do: {:error, :wrong_arguments}
  def get_balance(<<user::binary>>, <<currency::binary>>), do: User.get_balance(user, currency)
  def get_balance(_user, _currency), do: {:error, :wrong_arguments}

  @doc """
  - Decreases from_user's balance in given currency by amount value
  - Increases to_user's balance in given currency by amount value
  - Returns balance of from_user and to_user in given format
  """
  @spec send(
          from_user :: String.t(),
          to_user :: String.t(),
          amount :: number,
          currency :: String.t()
        ) :: ex_banking_success | ex_banking_error
  def send("", _to_user, _amount, _currency), do: {:error, :wrong_arguments}
  def send(_from_user, "", _amount, _currency), do: {:error, :wrong_arguments}
  def send(_from_user, _to_user, _amount, ""), do: {:error, :wrong_arguments}

  def send(<<from_user::binary>>, <<to_user::binary>>, amount, <<currency::binary>>)
      when is_number(amount) and amount >= 0,
      do: User.send(from_user, to_user, amount, currency)

  def send(_from_user, _to_user, _amount, _currency), do: {:error, :wrong_arguments}
end
