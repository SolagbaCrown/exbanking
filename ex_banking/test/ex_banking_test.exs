defmodule ExBankingTest do
  use ExUnit.Case
  doctest ExBanking
  import ExBanking

  setup do
    Application.stop(:ex_banking)
    Application.start(:ex_banking)

    user = "test_user"
    create_user(user)

    [user: user]
  end

  describe "create_user/1" do
    test "create user returns wrong arguments" do
      users = [nil, {}, "", 42, []]

      users
      |> Enum.each(fn user -> assert create_user(user) === {:error, :wrong_arguments} end)
    end

    test "create user successfully" do
      users = ["test_user2", "test_user3", "test user", "42"]

      users
      |> Enum.each(fn user -> assert create_user(user) === :ok end)
    end

    test "create user returns user already exists", context do
      assert create_user(context[:user]) === {:error, :user_already_exists}
    end
  end

  describe "deposit/3" do
    test "deposit returns wrong arguments for user" do
      users = [nil, {}, "", 42, []]

      users
      |> Enum.each(fn user -> assert deposit(user, 42, "EUR") === {:error, :wrong_arguments} end)
    end

    test "deposit returns wrong arguments for amount", context do
      amounts = [nil, {}, "", "42", []]

      amounts
      |> Enum.each(fn amount ->
        assert deposit(context[:user], amount, "USD") === {:error, :wrong_arguments}
      end)
    end

    test "deposit returns wrong arguments for currency", context do
      currencies = [nil, {}, "", 42, []]

      currencies
      |> Enum.each(fn currency ->
        assert deposit(context[:user], 42, currency) === {:error, :wrong_arguments}
      end)
    end

    test "deposit returns error if user does not exist" do
      assert deposit("test_user_does_not_exist", 1, "USD") ===
               {:error, :user_does_not_exist}
    end

    test "deposit money and get new balance in expected format", context do
      user = context[:user]
      currency = "EUR"
      deposit(user, 42, currency)

      assert get_balance(user, currency) === {:ok, 42.00}

      deposit(user, 24.666, currency)

      assert get_balance(user, currency) === {:ok, 66.67}
    end
  end

  describe "withdraw/3" do
    test "withdraw returns wrong arguments for user" do
      users = [nil, {}, "", 42, []]

      users
      |> Enum.each(fn user -> assert withdraw(user, 42, "EUR") === {:error, :wrong_arguments} end)
    end

    test "withdraw returns wrong arguments for amount", context do
      amounts = [nil, {}, "", "42", []]

      amounts
      |> Enum.each(fn amount ->
        assert withdraw(context[:user], amount, "USD") === {:error, :wrong_arguments}
      end)
    end

    test "withdraw returns wrong arguments for currency", context do
      currencies = [nil, {}, "", 42, []]

      currencies
      |> Enum.each(fn currency ->
        assert withdraw(context[:user], 42, currency) === {:error, :wrong_arguments}
      end)
    end

    test "withdraw returns error if user does not exist" do
      assert withdraw("test_user_does_not_exist", 1, "USD") === {:error, :user_does_not_exist}
    end

    test "withdraw without sufficient fund returns not enough", context do
      user = context[:user]

      assert withdraw(user, 42, "EUR") === {:error, :not_enough_money}

      deposit(user, 42, "EUR")
      withdraw(user, 41.11, "EUR")

      assert get_balance(user, "EUR") === {:ok, 0.89}
      assert withdraw(user, 2, "EUR") === {:error, :not_enough_money}
    end
  end

  describe "get_balance/2" do
    test "get balance returns wrong arguments for user" do
      users = [nil, {}, "", 42, []]

      users
      |> Enum.each(fn user -> assert get_balance(user, "EUR") === {:error, :wrong_arguments} end)
    end

    test "get balance returns wrong arguments for currency", context do
      currencies = [nil, {}, "", 42, []]

      currencies
      |> Enum.each(fn currency ->
        assert get_balance(context[:user], currency) === {:error, :wrong_arguments}
      end)
    end

    test "get balance returns error if user does not exist" do
      assert get_balance("test_user_does_not_exist", "USD") === {:error, :user_does_not_exist}
    end

    test "returns balance in expected format", context do
      user = context[:user]
      deposit(user, 42.13333333, "EUR")

      assert get_balance(user, "EUR") === {:ok, 42.13}

      assert get_balance(user, "USD") === {:ok, 0.00}
    end
  end

  describe "send/4" do
    test "send returns wrong arguments for user", context do
      users = [nil, {}, "", 42, []]

      users
      |> Enum.each(fn user ->
        assert send(user, context[:user], 100, "EUR") === {:error, :wrong_arguments}
      end)
    end

    test "send returns wrong arguments for amount", context do
      amounts = [nil, {}, "", "42", []]

      amounts
      |> Enum.each(fn amount ->
        assert send(context[:user], "user", amount, "USD") === {:error, :wrong_arguments}
      end)
    end

    test "send returns wrong arguments for currency", context do
      currencies = [nil, {}, "", 42, []]

      currencies
      |> Enum.each(fn currency ->
        assert send(context[:user], "user", 100, currency) === {:error, :wrong_arguments}
      end)
    end

    test "send returns error if user does not exist", context do
      user = context[:user]
      user_2 = "test_user_does_not_exist"
      assert send(user, user_2, 1, "USD") === {:error, :receiver_does_not_exist}
      assert send(user_2, user, 1, "USD") === {:error, :sender_does_not_exist}
    end

    test "send returns error for insufficient fund", context do
      user = context[:user]
      deposit(user, 10, "EUR")
      user_2 = "test_user_2"
      create_user(user_2)
      deposit(user_2, 10, "EUR")

      assert send(user, user_2, 100, "EUR") === {:error, :not_enough_money}
    end

    test "send - successful", context do
      user = context[:user]
      user_2 = "test_user2"
      create_user(user_2)
      deposit(user, 10, "EUR")

      assert send(user, user_2, 4, "EUR") == {:ok, 6.00, 4.00}

      assert get_balance(user, "EUR") === {:ok, 6.00}
      assert get_balance(user_2, "EUR") === {:ok, 4.00}

      deposit(user, 10, "EUR")

      assert send(user_2, user, 4, "EUR") == {:ok, 0.00, 20.00}

      assert get_balance(user, "EUR") === {:ok, 20.00}
      assert get_balance(user_2, "EUR") === {:ok, 0.00}
    end
  end

  describe "performance tests" do
    test "should handle 10 or less operations", context do
      user = context[:user]
      currency = "EUR"

      result =
        Enum.reduce(0..19, [], fn _, acc ->
          [Task.async(fn -> deposit(user, 1, currency) end) | acc]
        end)
        |> Enum.map(&Task.await/1)

      ok_result = Enum.count(result, fn {res, _} -> res == :ok end)
      error_result = Enum.count(result, fn {res, _} -> res == :error end)

      assert ok_result === 10
      assert error_result === 10
    end

    test "too many request - deposit", context do
      user = context[:user]
      currency = "EUR"

      result =
        Enum.map(1..11, fn _ -> Task.async(fn -> deposit(user, 1, currency) end) end)
        |> Enum.map(&Task.await/1)

      assert Enum.member?(result, {:error, :too_many_requests_to_user})
    end

    test "too many request - withdrawal", context do
      user = context[:user]
      currency = "EUR"

      deposit(user, 15, currency)

      result =
        Enum.map(1..11, fn _ -> Task.async(fn -> withdraw(user, 1, currency) end) end)
        |> Enum.map(&Task.await/1)

      assert Enum.member?(result, {:error, :too_many_requests_to_user})
    end

    test "too many request - sender", context do
      user = context[:user]
      currency = "EUR"

      user_2 = "user_2"

      deposit(user, 15, currency)

      create_user(user_2)

      result =
        Enum.map(1..11, fn _ ->
          Task.async(fn -> send(user, user_2, 1, currency) end)
        end)
        |> Enum.map(&Task.await/1)

      assert Enum.member?(result, {:error, :too_many_requests_to_sender}) == true
    end

    test "too many request - receiver", context do
      user = context[:user]
      currency = "EUR"

      user_2 = "user_2"
      user_3 = "user_3"
      user_4 = "user_4"

      deposit(user, 15, currency)

      create_user(user_2)

      create_user(user_3)
      create_user(user_4)

      deposit(user_2, 15, currency)
      deposit(user_3, 15, currency)
      deposit(user_4, 15, currency)

      result =
        Enum.map(1..19, fn i ->
          Task.async(fn ->
            cond do
              i < 7 -> send(user_2, user, 1, currency)
              i < 14 -> send(user_3, user, 1, currency)
              i < 20 -> send(user_4, user, 1, currency)
            end
          end)
        end)
        |> Enum.map(&Task.await/1)

      assert Enum.member?(result, {:error, :too_many_requests_to_receiver})
    end
  end
end
