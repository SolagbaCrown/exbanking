defmodule ExBanking.FormatterTest do
  use ExUnit.Case
  doctest ExBanking.Formatter

  import ExBanking.Formatter

  test "format integer amount" do
    assert format(1) === 1.00
  end

  test "format float amount" do
    assert format(1.0) === 1.00
  end
end
